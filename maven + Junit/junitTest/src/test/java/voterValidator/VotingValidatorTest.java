package voterValidator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class VotingValidatorTest
{
	private static VoterValidator voterValidator;

	@BeforeAll
	public static void setUp() {
		voterValidator = new VoterValidator();
	}

	@AfterAll
	public static void cleanUp() {
		voterValidator=null;
	}



	@Test
	@Order(1)
	@Tag("dev")
	public void validateVoterAgeValidTest() throws Exception {
		int age = 18;
		boolean result = voterValidator.validateVoterAge(age);
		assertTrue(result);
	}

	@Test
	@Order(2)
	@Tag("dev")
	public void validateVoterAgeInvalidTest() throws Exception {
		int age = 17;
		boolean result = voterValidator.validateVoterAge(age);
		assertFalse(result);
	}

	@Test
	@Order(3)
	@Tag("prod")
	public void validateVoterAgeInvalidTest2() throws Exception {
		int age = -14;

		Exception exception = assertThrows(Exception.class, () -> voterValidator.validateVoterAge(age));
		assertEquals("Invalid age", exception.getMessage());
	}
//
	@Tag("test")
	@Order(4)
	@ParameterizedTest
	@CsvSource(value = {"19, true","20,true","17,false","16,false"})
	public void validateVoterAgeTestParameter(int age, boolean expected) throws Exception {

		boolean actual = voterValidator.validateVoterAge(age);
		assertEquals(expected, actual);
	}



}


