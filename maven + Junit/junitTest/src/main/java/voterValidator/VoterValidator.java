package voterValidator;

import execptions.InvalidVoterException;

public class VoterValidator {
	
	public boolean validateVoterAge(int age) throws Exception 
	{
		boolean result=false;
		
		if(age<0)
			throw new InvalidVoterException("Invalid age");
		if(age >= 18) {
			result= true;
		}
		else {
			result = false;
		}
		return result;
	}
	public boolean validateVoterAge(int age,boolean expected) throws Exception 
	{
		if(age<0)
			throw new RuntimeException("Invalid age");
		if(age >= 18) {
			return true;
		}
		else {
			return false;
		}
	}
}