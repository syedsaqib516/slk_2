package com.hcl.filter;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.hcl.util.JwtUtil;

@Component
public class JwtFilter extends OncePerRequestFilter
{
	@Autowired
	JwtUtil jwtUtil;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException 
	{
		String authorizationHeader=request.getHeader("Authorization");  //Bearer ey8wyquedkgcfbhfcagjhgtrjhgrylrsgxbhdafhdchagfaye
		String token=null;
		String username=null;
		
		if(authorizationHeader!=null && authorizationHeader.startsWith("Bearer "))
		{
			token=authorizationHeader.substring(7);
			System.out.println(token);
			username=jwtUtil.extractUsername(token);
		}
		if(username!=null && SecurityContextHolder.getContext().getAuthentication()==null)
		{
			UserDetails userDetails=User.withUsername(username).password("hcl123").roles("owner").build();
			UsernamePasswordAuthenticationToken authToken=new UsernamePasswordAuthenticationToken(userDetails,null,new ArrayList<>());
			authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
			SecurityContextHolder.getContext().setAuthentication(authToken);		
		}
		filterChain.doFilter(request, response);
	}
}
