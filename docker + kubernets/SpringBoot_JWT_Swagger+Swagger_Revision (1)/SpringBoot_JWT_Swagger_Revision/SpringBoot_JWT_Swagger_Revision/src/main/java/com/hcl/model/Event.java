package com.hcl.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Entity
@Table(name="Event_Details")
public class Event 
{	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int eid;
	public Event(int eid, String ename, String etype, int hours, double amount) {
		super();
		this.eid = eid;
		this.ename = ename;
		this.etype = etype;
		this.hours = hours;
		this.amount = amount;
	}
	public int getEid() {
		return eid;
	}
	public void setEid(int eid) {
		this.eid = eid;
	}
	public String getEname() {
		return ename;
	}
	public void setEname(String ename) {
		this.ename = ename;
	}
	public String getEtype() {
		return etype;
	}
	public void setEtype(String etype) {
		this.etype = etype;
	}
	public int getHours() {
		return hours;
	}
	public void setHours(int hours) {
		this.hours = hours;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	private String ename;
	private String etype;
	private int hours;
	private double amount;
	
}
