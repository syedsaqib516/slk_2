package com.hcl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootJwtSwaggerRevisionApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJwtSwaggerRevisionApplication.class, args);
	}

}
