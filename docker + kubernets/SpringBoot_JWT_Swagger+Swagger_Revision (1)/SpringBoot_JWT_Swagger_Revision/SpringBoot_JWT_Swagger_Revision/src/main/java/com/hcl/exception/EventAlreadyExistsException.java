package com.hcl.exception;

public class EventAlreadyExistsException extends RuntimeException 
{
	private String msg;
	public EventAlreadyExistsException(String msg)
	{
		super(msg);
		this.msg=msg;
	}
}
