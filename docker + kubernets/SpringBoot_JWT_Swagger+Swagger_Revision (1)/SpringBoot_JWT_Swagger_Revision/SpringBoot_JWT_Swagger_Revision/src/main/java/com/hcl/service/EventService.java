package com.hcl.service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.exception.EventAlreadyExistsException;
import com.hcl.exception.EventNotFoundException;
import com.hcl.model.Event;
import com.hcl.repository.EventRepository;

@Service
public class EventService 
{
	@Autowired
	EventRepository eventRepo;
	
	public Event addEvent(Event eve)
	{
		boolean eventExists=eventRepo.existsById(eve.getEid());
		if(eventExists==false)
		{
			return eventRepo.save(eve);
		}
		else
		{
			throw new EventAlreadyExistsException("Event with id "+eve.getEid()+" already exists");
		}
	}
	
	public List<Event> getAllEvents()
	{
		return eventRepo.findAll();
	}
	
	public Event getEventById(int id)
	{
		Optional<Event> op=eventRepo.findById(id);
		try
		{
			return op.get();
		}
		catch(NoSuchElementException ex)
		{
			throw new EventNotFoundException("Event with id "+id+" does not exist");
		}
	}
}
