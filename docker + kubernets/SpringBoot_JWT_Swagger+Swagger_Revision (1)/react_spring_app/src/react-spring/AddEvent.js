import { useState } from "react";
import axios from "axios";

function AddEvent()
{
  const[eid,setEid]=useState();
  const[ename,setEname]=useState('');
  const[etype,setEtype]=useState('');
  const[amount,setAmount]=useState();
  const[hours,setHours]=useState();

  const onChangeId = (event) => {
                                    setEid(event.target.value);
                                }
  const onChangeEname = (event) => {
                                    setEname(event.target.value);
                                   }
  const onChangeEtype = (event) => {
                                    setEtype(event.target.value);
                                   }
  const onChangeAmount = (event) => {
                                            setAmount(event.target.value);
                                         }
  const onChangeHours = (event) => {
                                            setHours(event.target.value);
                                         }                                       
  const onBtnClick = (event) => {
                                    event.preventDefault();
                                    //console.log("Button click");
                                    const eventobj={eid,ename,etype,hours,amount};
                                    axios.post("http://localhost:8081/eve/addEvent/event",eventobj)
                                    .then(response => {console.log(response.data);
                                                    alert("Event data inserted successfully");})
                                    .catch(error => console.log("Server Issue: "+error))
                                }
  return (
            <>
                <br/>
                <form className="addEventForm">
                    <br/>
                    <input type="number" placeholder="Enter Id" value={eid} onChange={onChangeId}></input>
                    <br/><br/>
                    <input type="text" placeholder="Enter Name" value={ename} onChange={onChangeEname}></input>
                    <br/><br/>
                    <input type="text" placeholder="Enter Type" value={etype} onChange={onChangeEtype}></input>
                    <br/><br/>
                    <input type="text" placeholder="Enter Hours" value={hours} onChange={onChangeHours}></input>
                    <br/><br/>
                    <input type="text" placeholder="Enter Amount" value={amount} onChange={onChangeAmount}></input>
                    <br/><br/>
                    <button onClick={onBtnClick}>Submit Event Details</button>
                </form>
            </>
         );
}
export default AddEvent;