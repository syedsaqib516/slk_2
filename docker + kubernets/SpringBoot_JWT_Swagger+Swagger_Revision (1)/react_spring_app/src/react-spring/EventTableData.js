import axios from "axios";
import { useState } from "react";

function EventTableData()
{
    const [events,setEvents]=useState([]);

    const getAllEvents = () =>
    {
        axios.get('http://localhost:8081/eve/allevents')
        .then((response) => {
            
                                setEvents(response.data)
                                console.log(response.data)
                            })
        .catch((error) => {
                            console.log('Response Error: '+error);
        })
    }
   

    return (<div>
            <h2>Event Details</h2>
            <button onClick={getAllEvents}>Fetch Event Details</button>
            <br/><br/>
            <div className="tbldiv" style={{textAlign: "center"}}>
            <table border="1">
                <thead>
                    <tr><th colSpan="5">Event Details</th></tr>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Hours</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                 <tbody>
                 {
                    events.map((event) => (
                        <tr>
                            <td>{event.eid}</td>
                            <td>{event.ename}</td>
                            <td>{event.etype}</td>
                            <td>{event.hours}</td>
                            <td>{event.amount}</td>
                        </tr>
                    ))
                 }
                 </tbody>
            </table>


            </div>
            </div>);

}
export default EventTableData;