import logo from './logo.svg';
import './App.css';
import EventTableData from './react-spring/EventTableData';
import AddEvent from './react-spring/AddEvent';

function App() {
  return (
    <div className="App">
        <EventTableData/>
        <AddEvent/>
    </div>
  );
}

export default App;
