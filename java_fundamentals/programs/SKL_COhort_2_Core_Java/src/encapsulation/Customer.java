package encapsulation;

public class Customer 
{
	private String custId;
	private String custEmail;
	private String custName;

	public Customer(String custId, String custEmail, String custName) {
		super();
		this.custId = custId;
		this.custEmail = custEmail;
		this.custName = custName;
	}
	
	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCustEmail() {
		return custEmail;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}
	public String getCustId()
	{
		return custId;
	}

	public void setCustEmail(String custEmail)
	{
		this.custEmail=custEmail;
	}
	@Override
	public String toString() {
		return "Customer [custId=" + custId + ", custEmail=" + custEmail + ", custName=" + custName + "]";
	}

}
