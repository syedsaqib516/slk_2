package encapsulation;

public class CustMain {
	public static void main(String[] args) {

		Customer c=new Customer("123","abcd@gmail.com","abcd");

		System.out.println(c.getCustId());
		
		c.setCustEmail("abcd@yahoo.com");
		System.out.println(c.getCustEmail());
		
		System.out.println(c);
		
		
	}
}
