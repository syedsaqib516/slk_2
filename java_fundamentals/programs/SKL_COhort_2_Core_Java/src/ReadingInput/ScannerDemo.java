package ReadingInput;
import java.util.Scanner;
public class ScannerDemo 
{
	public static void main(String[] args) 
	{
       Scanner scan = new Scanner(System.in);
       System.out.println("enter your text");
       String s=scan.nextLine();
       System.out.println(s);
       
       scan.nextByte();
       scan.nextShort();
       scan.nextLong();
       scan.nextBoolean();
       scan.nextDouble();
       
       char c=   scan.next().charAt(0);
       
	}
}
