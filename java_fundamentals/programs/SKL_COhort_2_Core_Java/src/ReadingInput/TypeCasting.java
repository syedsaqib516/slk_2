package ReadingInput;

public class TypeCasting {
	public static void main(String[] args) {

		int x=10;
		double d= x;// implicit/ auto t.c
		
		double d1= 20.55;
		int x1=(int)d1;
		System.out.println(x1);
		
		char c= 'a';
		int x2=c;
		System.out.println(x2);
		
		char c2=(char)x2;
		System.out.println(c2);
	}
}
