package polymorphism;

class X
{
	
}
class Y extends X
{
	final double PI=3.14;
}
 class Parent 
{
	static public Y getMarried()
	{
		System.out.println("Marry Murthy");
		return null;
	}
}
class Child extends Parent
{
	
	static public Y getMarried()
	{
		System.out.println("Marry Damon");
		return null;
	}

}

public class OverridingDemo
{
	public static void main(String[] args) {
		Child c = new Child();
		Child.getMarried();
	}
}
