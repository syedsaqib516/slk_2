package polymorphism;

class A
{
	void m1()
	{
		System.out.println("m1 from A");
	}
	void m2()
	{
		System.out.println("m2 from A");
	}
}
class B extends A
{
	void m2()
	{
		System.out.println("m2 from B");
	}
	void m3()
	{
		System.out.println("m3 of B");
	}
}
class C extends A
{
	void m2()
	{
		System.out.println("m2 from C");
	}
	void m3()
	{
		System.out.println("m3 of C");
	}
}


public class Demo3 
{
	public static void main(String[] args)
	{
//		A a = new A();
//		a.m1();
//		a.m2();
//		B b = new B();
//		b.m1();
//		b.m2();
		
		A a = new B();// upcasting/lose couple parent ref to child object
		a.m1();
		a.m2();
		((B)(a)).m3();// downcasting to access special methods of child class
		a= new C();
		a.m1();
		a.m2();
		((C)(a)).m3();

	}
}
