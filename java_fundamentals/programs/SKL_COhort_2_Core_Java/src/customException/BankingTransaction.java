package customException;

public class BankingTransaction
{
	public void validateBalance(double amtBal,double amtWithdraw)
	{
		System.out.println("validate method started");
		
		if(amtBal>amtWithdraw)
		{
			System.out.println("withdrawl successful, please collect cash!");
			amtBal= amtBal-amtWithdraw;
			
		}
		else
			try {
				throw new InsufficientBalanceException("Withdraw within your Aukat");
				
			} catch (RuntimeException e) {
				System.out.println(e.getMessage());
			}
		 System.out.println("Yur availible balance is = "+amtBal);
		System.out.println("validate method ended");
	}

}
