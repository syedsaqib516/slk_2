package collection;

import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.TreeSet;

class MyOrder implements Comparator
{

	@Override
	public int compare(Object o1, Object o2) {
		Integer i1= (Integer)o1;
		Integer i2= (Integer)o2;
		return - i1.compareTo(i2);
	}
	
}

public class Demo2 
{
	public static void main(String[] args) 
	{
		LinkedHashSet hs = new LinkedHashSet();
		hs.add(10);
		hs.add(10);
		hs.add("abc");
		hs.add(10.22);
		System.out.println(hs);
		Integer i;
		TreeSet<Integer> ts = new TreeSet<Integer>(new MyOrder());// 50 100 150 25 75 125 200
		ts.add(50);
		ts.add(100);
		ts.add(150);
		ts.add(25);
		ts.add(75);
		ts.add(125);
		ts.add(200);
		System.out.println(ts);//comparebale if implements comparable compareTo
		
		//25 50 75 100 125 150 200
	}

}
