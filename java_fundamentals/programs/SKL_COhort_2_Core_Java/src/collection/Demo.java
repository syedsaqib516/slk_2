package collection;

public class Demo 
{
	public static void main(String[] args) 
	{
		int x=10;
		//Integer x1=10;//auto boxing/wrapping
		Integer x1=x;// auto boxing/wrapping
		
		int x2= x1;// auto unboxing/unwrapping
		
		Integer x3=Integer.valueOf(x);// explicit boxing/wrapping
		int x4 = x1.intValue();// explicit unboxing/unwrapping
		 
		
	}

}
