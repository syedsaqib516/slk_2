package collection;

import java.util.Comparator;
import java.util.TreeMap;
class MyOrder1 implements Comparator
{

	@Override
	public int compare(Object o1, Object o2) {
		Integer i1= (Integer)o1;
		Integer i2= (Integer)o2;
		return - i1.compareTo(i2);
	}
	
}
public class Demo3 
{
	public static void main(String[] args) {
		TreeMap<Integer,String> hm = new TreeMap<Integer,String> (new MyOrder1());
		hm.put(2, "def");
		hm.put(3, "213");
		hm.put(4, "1234");
		hm.put(1, "abc");
		System.out.println(hm);
		System.out.println(hm.get(1));
	}

}
