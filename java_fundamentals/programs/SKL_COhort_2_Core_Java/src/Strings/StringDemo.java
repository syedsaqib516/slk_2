package Strings;

public class StringDemo 
{
	 public static void main(String[] args) 
	 {
		 int x= 10;
		 
		 String pwd= "saqib@123";
		 String userPwd= "adfdui";
		 String s1="hello";
		 
		 String s2 = new String("HELLO");
		
		 
		char c= s1.charAt(0);
		System.out.println(c);
		
		System.out.println("a".codePointAt(0));
		
		System.out.println(pwd.compareTo(userPwd));
		System.out.println(s1.compareToIgnoreCase(s2));
		System.out.println(s1.equals(s2));
		System.out.println(s1.equalsIgnoreCase(s2));
		
		System.out.println("abc"+"def");
		System.out.println("abc".concat(s2));
		System.out.println(s1.concat(s2));
		
		
		System.out.println(pwd.substring(6)); 
		System.out.println(pwd.substring(0,5));
		
		char[] a=pwd.toCharArray();
		
		String name= " rajarammohanroy ";
		System.out.println(name.toUpperCase().toLowerCase());
		System.out.println(name.trim());
		
	 }
	 
	 
	

}
