package Strings;
import java.util.Scanner;
import java.util.Scanner;

class Circle {
    private double radius;

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return Math.PI * radius * radius;
    }

    public double getCircumference() {
        return 2 * Math.PI * radius;
    }
}

public class Main{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Circle circle = new Circle();

        System.out.print("Enter the radius of the circle: ");
        double radius = scanner.nextDouble();

        circle.setRadius(radius);

        System.out.println("Area of the circle: " + circle.getArea());
        System.out.println("Circumference of the circle: " + circle.getCircumference());

        scanner.close();
    }
}
