package thisAndSuper;

//public class Object{
//	public Object()
//	{
//		
//	}
//}

class X
{
	X(int x)
	{
		super();
		System.out.println(" X No arg constr");
	}
}
class Y extends X
{
	Y()
	{
		super(10);
		System.out.println("Y No arg constr");
	}
}

public class SuperDemo
{
	public static void main(String[] args) {
		new Y();
	}

}
