package thisAndSuper;


class A 
{
    A()
    {
    	this(10);
    	System.out.println("no arg constructor");
    }
    
    A(int x)
    {
    	this(10.11);
    	System.out.println("int arg constructor");	
    }
    A(double x)
    {
    	this(10,10.11);
    	System.out.println("double arg constructor");
    }
    A(int x , double d)
    {
    	System.out.println("int double arg constructor");
    }
    
}


public class ThisDemo 
{
	public static void main(String[] args)
	{
         new A();
	}
}
