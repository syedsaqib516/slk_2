package Arrays;

import java.util.Scanner;

public class Array2D 
{
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		int[][] marks = new int[3][3];

		System.out.println("enter the marks ");

		for (int i = 0; i < marks.length; i++) 
		{
			for(int j=0;j< marks[i].length;j++)
			{
				System.out.println("enter the marks of "+i+" class "+j+" student");
				marks[i][j]=scan.nextInt();
			}

		}

		System.out.println("the marks are");

		for (int i = 0; i < marks.length; i++) 
		{
			for(int j=0;j< marks[i].length;j++)
			{
				System.out.print(marks[i][j]+" ");
			}
           System.out.println();
		}
	}
}

