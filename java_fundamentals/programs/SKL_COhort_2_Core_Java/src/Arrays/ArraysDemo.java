package Arrays;

public class ArraysDemo 
{
	public static void main(String[] args) 
	{
		//1-D array
		String[] names= {"abcd","efgh","ijkl"};
		
		//1-D array
		int[] marks = new int[7];
		marks[0]=10;
		marks[1]=20;
		
		//2-D array
	   String[][] names2= {{"abcd","efgh","ijkl"},{"abcd","efgh"},{"abcd","efgh","ijkl"}};
				
	  //1-D array
//     int[][] marks2 = new int[3][3];
//	marks2[0][0]=10;
//	marks2[0][1]=20;
	   
	   int[][] marks2 = new int[3][];
		marks2[0]=new int[4];
		marks2[1]=new int[5];
		marks2[2]=new int[3];
		
		
	}

}
