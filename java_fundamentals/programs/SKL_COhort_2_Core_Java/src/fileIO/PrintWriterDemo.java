package fileIO;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import encapsulation.Customer;

public class PrintWriterDemo
{
	public static void main(String[] args) throws FileNotFoundException {
		
		PrintWriter pw = new PrintWriter("output.txt");		
		pw.write("hi");
		pw.write(100);
		pw.print(100);
		pw.println();
	    char[] c=	{'a','b','c'};
		pw.println(c);
		
		Customer cust= new Customer("1ds2","ahfga@gmail.com","namss");
		pw.print(cust);
		pw.flush();
	}

}
