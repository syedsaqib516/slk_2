package fileIO;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class IOStream
{
	public static void main(String[] args) throws IOException
	{
		
		FileInputStream fis = new FileInputStream("input.txt");
		BufferedInputStream bis = new BufferedInputStream(fis);
		FileOutputStream fos = new FileOutputStream("output.txt");
		BufferedOutputStream bos = new BufferedOutputStream(fos);
		
		int temp;
		
		while((temp=fis.read())!=-1) {
			fos.write(temp);
		}
		
	}

}
