package fileIO;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileWR 
{
	public static void main(String[] args) throws IOException
	{
		FileReader fis = new FileReader("input.txt");
		BufferedReader bis = new BufferedReader(fis);
		FileWriter fos = new FileWriter("output.txt");
		BufferedWriter bos = new BufferedWriter(fos);
		
		String temp;
		
		while((temp=bis.readLine())!=null) {
			bos.write(temp);
		}
		
		//fos.flush();
		//fos.close();
		bos.flush();
	}

}
