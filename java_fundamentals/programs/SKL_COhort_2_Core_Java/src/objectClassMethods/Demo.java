package objectClassMethods;

import java.util.Objects;

@MyAnnotation
class Test
{
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Test other = (Test) obj;
		return x == other.x;
	}

	int x;
	
	public Test(int x) {
		super();
		this.x = x;
	}
	@Override
	public int hashCode() {
		return Objects.hash(x);
	}
	
	@Override
	public String toString() {
		return "Test [x=" + x + "]";
	}
}

public class Demo
{
	public static void main(String[] args)
	{
		Object obj = new Object();
		Object obj1 = new Object();
		Object obj2=obj1;
		System.out.println(obj.toString());
		System.out.println(obj.hashCode());
		System.out.println(obj.equals(obj1));
		System.out.println(obj1.equals(obj2));
		
		String s= "hello";
		String s1="hello";
		System.out.println(s.toString());
		System.out.println(s.equals(s1));
		
		Test t = new Test(10);
		System.out.println(t.toString());
		System.out.println(t.hashCode());
		
		System.out.println(s.getClass());
		System.out.println(s.getClass().getName());
		
		
	}
}
