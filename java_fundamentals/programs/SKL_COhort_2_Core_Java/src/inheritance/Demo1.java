package inheritance;


class A 
{
	public int x =10;
	static int y = 20;
	public void m1()
	{
		System.out.println("non static method");
	}
	static  void m2()
	{
		System.out.println("static method");
	}
}
class B extends A
{
 
}
class C extends A
{
 
}
public class Demo1 
{
	public static void main(String[] args)
	{
      B b = new B();
      b.x=10;
      b.m1();
      B.y=10;
      B.m2();

	}

}
