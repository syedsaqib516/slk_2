package methods;

class Calculator
{
	void  methodName()
	{
		System.out.println("dummy method");
	}

	void add(int a,int b)
	{
		int sum = a+b;
		System.out.println("the sum is = "+sum);
	}
	void add(double a,int b)
	{
		double sum = a+b;
		System.out.println("the sum is = "+sum);
	}
	void add(int a,double b)
	{
		double sum = a+b;
		System.out.println("the sum is = "+sum);
	}

	int sub(int a,int b)
	{
		int diff = a-b;
		return diff;
	}

	public static strictfp double div(double a,double b)//parameters/arguments
	{
		double res = a/b;
		return a/b;
	}
}
public class Main 
{
	public static void main(String[] args) {
		Calculator c = new Calculator();

		c.methodName();
		c.add(10, 20);
		int result=	c.sub(20, 10);
		System.out.println("the diff is = "+result);
		System.out.println("the res = "+c.div(20,10));
		
		c.add(10,20);


	}
}
