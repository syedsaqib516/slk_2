package DateTimeapi;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class Demo1 
{
	public static void main(String[] args) 
	{
		LocalDate ld = LocalDate.now();
		System.out.println(ld);
		LocalDate birthdate= LocalDate.of(1996, 4, 19);
		System.out.println(birthdate);
		
		int yy=ld.getYear();
		int mm=ld.getMonthValue();
		int dd=ld.getDayOfMonth();
		System.out.printf("%d/%d/%d",dd,mm,yy);
		System.out.println();
		LocalTime lt = LocalTime.now();
		System.out.println(lt);
		LocalTime ltn = LocalTime.of(19,55);
		System.out.println(ltn);
		
		LocalDateTime ldt =LocalDateTime.now();
		System.out.println(ldt);
		
		
		Period p = Period.between(ld, birthdate);
		System.out.println(p.getYears());
		
		ZoneId id = ZoneId.of("America/Chicago");
		ZonedDateTime zdt = ZonedDateTime.now(id);
		System.out.println(zdt);
	}
	

}
