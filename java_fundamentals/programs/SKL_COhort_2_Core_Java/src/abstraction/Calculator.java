package abstraction;

public interface Calculator 
{
	int x=10;
	void add();
	
//	public void logs()
//	{
//		System.out.println("logs functionality");
//	}

}

@FunctionalInterface
interface A{
	void m1();
	
}
interface B{
	void m2();
}
interface C extends A,B{
	
}