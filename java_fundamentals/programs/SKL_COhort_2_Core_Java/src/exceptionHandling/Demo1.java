package exceptionHandling;

import java.io.File;
import java.io.IOException;
import java.util.InputMismatchException;

public class Demo1 
{
	public static void main(String[] args) 
	{
		System.out.println("main start");

		try {
			System.out.println("try start");
			System.out.println(10/0);// new ArithmeticException();
			File f = new File("/xyz/");
			try {
				f.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("try end");
		} 

		catch (ArithmeticException e)
		//specific catch block
		{// ArithmeticException e = new ArithmeticException();
			System.out.println("please enter non 0 denomination "+e.getLocalizedMessage());

		}
		catch (InputMismatchException e) {
			System.out.println("please enter numbers "+e.getLocalizedMessage());
		}
		catch (ArrayIndexOutOfBoundsException  e) {
			System.out.println("please use within index range "+e.getLocalizedMessage());
		}
		catch (Exception  e) // upcasting/ lose coupling
		{
			// generic
			System.out.println("something went wrong please try again with valid inputs"+e.getLocalizedMessage());
		}

		System.out.println("main end");
	}

}
