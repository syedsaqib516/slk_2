package exceptionHandling;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Demo3 
{
	public static void main(String[] args) throws FileNotFoundException 
	{
		System.out.println("main start");
		m1();
		System.out.println("main end");
		
	}

	private static void m1() throws FileNotFoundException {
		System.out.println("m1 start");
		m2();
		System.out.println("m1 end");
		
	}

	private static void m2() throws FileNotFoundException {
		System.out.println("m2 start");
		try {
			m3();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("m2 end");
	}

	private static void m3() throws FileNotFoundException,IOException,Exception{
		System.out.println("m3 start");
	
		throw new FileNotFoundException();
		
	}

}
