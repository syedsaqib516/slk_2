package exceptionHandling;

public class Demo4
{
	public static void main(String[] args) {
		
		try {
			// opened a file / db connection
			System.out.println(10/0);
		}
		finally {
			// f.close()/ dbCon.close();
			System.out.println("finally executed");
		}
	}

}
