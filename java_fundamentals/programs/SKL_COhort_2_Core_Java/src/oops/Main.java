package oops;

class Employee
{
	String empName;// non static / instance variables
	int empId;
	double empSalary;
	long empContact;
	static final String COMPANY_NAME="SLK";// static/class variables


	static public void logging(int z)// static method
	{
		System.out.println("logged in/out successful");
		int x;// local
		//System.out.println(x); local cannot be used without initialization.
	   //System.out.println(empName);
		System.out.println(COMPANY_NAME);
	}

	public void developmentWork()// non static mentod
	{
		System.out.println("coding....");
		//static int y=10;  // CTE local variables cannot be static
		System.out.println(COMPANY_NAME);
	}	
}


public class Main 
{


	public static void main(String[] args) 
	{
		Employee.logging(0) ;
		System.out.println(Employee.COMPANY_NAME);

		Employee emp1=new Employee();// instantiation

		emp1.empId=111;
		emp1.empName="abcd";

       
		System.out.println(emp1.empId);
		System.out.println(emp1.empName);
		emp1.developmentWork();



	}	

}
