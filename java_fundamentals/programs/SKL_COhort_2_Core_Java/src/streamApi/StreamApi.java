package streamApi;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamApi {
	public static void main(String[] args)
	{
		ArrayList<Integer> nums = new ArrayList<Integer>();
		nums.add(5);
		nums.add(6);
		nums.add(7);
		nums.add(8);
		nums.add(9);
		nums.add(1);
		nums.add(2);
		nums.add(3);
		nums.add(4);
		
		System.out.println(nums);
		ArrayList<Integer> evenNums = new ArrayList<Integer>();
		for(Integer i : nums)
		{
			if(i%2==0)
			{
				evenNums.add(i);
			}
		}
		System.out.println(evenNums);
		
		
		List evenList =nums.stream().filter(n->n%2==0).collect(Collectors.toList());
		 
	    System.out.println(evenList);
		 
		 
		List extraMarksList= nums.stream().filter(n->n<5).map(n->n+4).collect(Collectors.toList());
		System.out.println(extraMarksList);
		 
		List sortedListAsc=	nums.stream().sorted().collect(Collectors.toList());
		System.out.println(sortedListAsc);
		
		List sortedListDsc=	nums.stream().sorted((i1,i2)->-i1.compareTo(i2)).collect(Collectors.toList());
		System.out.println(sortedListDsc);
		
		long numOfFailed= nums.stream().filter(n->n<5).count();
		System.out.println(numOfFailed);
		
		
		//nums.forEach(n->System.out.println(n));
		
		nums.forEach(System.out::println);
		
		Consumer<Integer> c= n->{
		System.out.println("the square of "+n+" is "+(n*n));
		};
		
		nums.forEach(c);
		
		
	Stream s=Stream.of(99,999,99999,9);
	
	Integer i[] = {1,2,3,4,5};
	
	Stream s1=Stream.of(i);
		
		
		
		
		
	
		
		
	}

}
