package lambdaExpressions;

import java.util.function.Predicate;

public class PredicateDemo 
{
	public static void main(String[] args)
	{
		Predicate<Integer> p= n->n%2==0;
		Predicate<String> p1= s->s.length()>5;
		
		System.out.println(p.test(19890));
		System.out.println(p1.test("saqib"));
		
	}

}
