package lambdaExpressions;

@FunctionalInterface //  SAM
interface Itr
{
	public int square(int n);
}

public class Demo 
{
	public static void main(String[] args) 
	{
		Itr i= n->n*n;
		System.out.println(i.square(10));		
	}
}
