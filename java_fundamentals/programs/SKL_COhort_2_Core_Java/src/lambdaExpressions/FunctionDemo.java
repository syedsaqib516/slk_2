package lambdaExpressions;

import java.util.function.Function;

public class FunctionDemo 
{
	public static void main(String[] args) {
		Function<Integer,Integer> f=n->n*n;
		System.out.println(f.apply(10));
		Function<String,Integer> f1= s->s.length();
		System.out.println(f1.apply("ajhsydyuwiuye"));
	}

}
