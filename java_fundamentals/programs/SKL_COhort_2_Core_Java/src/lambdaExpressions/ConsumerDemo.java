package lambdaExpressions;

import java.util.Date;
import java.util.function.Consumer;

public class ConsumerDemo
{
	public static void main(String[] args) 
	{
		Consumer<Date> c= d->System.out.println(d);
		
		c.accept(new Date());
		
	}

}
