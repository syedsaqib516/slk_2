package com.slk.eventsApp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.slk.eventsApp.beans.Events;
import com.slk.eventsApp.beans.SpecialEvents;
import com.slk.eventsApp.service.EventsService;

@RestController
@CrossOrigin(origins="http://localhost:4200")
@RequestMapping("/events")
public class EventsController {
	

	@Autowired
	EventsService eventsService;
	
	
	@GetMapping("/allEvents")
	public List<Events> getAllEvents()
	{
		return eventsService.getAllEvents();
	}
	
	@GetMapping("/allSpecialEvents")
	public List<SpecialEvents> getAllSpecialEvents()
	{
		return eventsService.getAllSpecialEvents();
	}

}
