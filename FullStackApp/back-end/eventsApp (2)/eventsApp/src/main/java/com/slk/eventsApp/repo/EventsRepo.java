package com.slk.eventsApp.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.slk.eventsApp.beans.Events;

public interface EventsRepo extends JpaRepository<Events,Integer> {

}
