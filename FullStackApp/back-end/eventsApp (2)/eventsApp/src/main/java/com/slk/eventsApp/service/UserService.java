package com.slk.eventsApp.service;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.slk.eventsApp.beans.User;
import com.slk.eventsApp.repo.UserRepo;

@Service
public class UserService 
{
	@Autowired
	 UserRepo repo;
	
//	@Autowired
//	 BCryptPasswordEncoder bpe;
	
	public void addUser(User user)
	{
//		//user.setPassword(bpe.encode(user.getPassword()));
		
		repo.save(user);
	}
	
	public User findUser(User user)
	{
//		//user.setPassword(bpe.encode(user.getPassword()));
		
		User userValid=repo.findByUserNameAndPassword(user.getUserName(),user.getPassword());
		if(userValid!=null)
		{
			return userValid;
		}
		else
		{
			return user;
		}
		
	}

}
