package com.slk.eventsApp.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.slk.eventsApp.beans.SpecialEvents;

public interface SpecialEventsRepo extends JpaRepository<SpecialEvents,Integer> {

}
