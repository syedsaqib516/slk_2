package com.slk.eventsApp.repo;

import org.springframework.data.repository.CrudRepository;

import com.slk.eventsApp.beans.User;

public interface UserRepo extends CrudRepository <User,String>
{

	User findByUserName(String userName);

	User findByUserNameAndPassword(String userName, String password);
	

}
