package com.slk.eventsApp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.slk.eventsApp.beans.Events;
import com.slk.eventsApp.beans.SpecialEvents;
import com.slk.eventsApp.repo.EventsRepo;
import com.slk.eventsApp.repo.SpecialEventsRepo;

@Service
public class EventsService 
{
	@Autowired
	EventsRepo eventsRepo;
	
	@Autowired
	SpecialEventsRepo specialEventsRepo;
	
	
	public List<Events> getAllEvents()
	{
		return eventsRepo.findAll();
	}
	
	
	public List<SpecialEvents> getAllSpecialEvents()
	{
		return specialEventsRepo.findAll();
	}

}
