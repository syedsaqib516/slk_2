package com.slk.eventsApp.controller;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//import com.hcl.loginDemo.beans.AuthRequest;
import com.slk.eventsApp.beans.User;
import com.slk.eventsApp.service.UserService;

@RestController
@CrossOrigin(origins="http://localhost:4200")
@RequestMapping("/user")
public class UserController 
{
	
//	@Autowired 
//	JwtUtil jwtUtil;
//	
	@Autowired
	UserService userService;
	
//	@Autowired
//	AuthenticationManager authenticationManager;
	
	@GetMapping("/")
	public String welcome()
	{
		return "Welcome Dear User";
	}
	@SuppressWarnings("rawtypes")
	@PostMapping("/register")
	public User  registerUser(@RequestBody User user) throws Exception
	{
	 userService.addUser(user);
		return user;
     }
	
	@SuppressWarnings("rawtypes")
	@PostMapping("/login")
	public User  loginUser(@RequestBody User user) throws Exception
	{
		String res=null;
      
//    	 if(  userService.findUser(user)!=null)
//    	 {
//    		 res= "login success";//jwtUtil.generateToken(authRequest.getUserName());
//    	 }
//    	 else {
//    		 res= "unsucessful login";
//    	 
//    	    
//    	 }
		return userService.findUser(user);
	}
	
	

}
