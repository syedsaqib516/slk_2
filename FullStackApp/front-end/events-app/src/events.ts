export class Events
{
    public name:string;
    public description:string;
    public date:Date;

    constructor(name:string, description:string,date:Date)
    {
        this.name=name;
        this.description=description;
        this.date=date;
    }
}