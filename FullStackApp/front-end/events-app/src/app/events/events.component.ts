import { Component } from '@angular/core';
import { Events } from 'src/events';
import { EventService } from '../event.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css'],
})
export class EventsComponent {
  events: Events[] = [
  //   { name: 'bday1', description: 'its my bday1', date: new Date('1996-2-1') },
  //   { name: 'bday2', description: 'its my bday2', date: new Date('1996-2-1') },
  //   { name: 'bday3', description: 'its my bday3', date: new Date('1996-2-1') },
  //   { name: 'bday4', description: 'its my bday4', date: new Date('1996-2-1') },
  //   { name: 'bday5', description: 'its my bday5', date: new Date('1996-2-1') },
  //   { name: 'bday6', description: 'its my bday6', date: new Date('1996-2-1') }
  // 
];
  constructor(private _eventService: EventService) { }

  ngOnInit() {
    this._eventService.getEvents()
      .subscribe(
        res => this.events = res,
      );
  }


}
