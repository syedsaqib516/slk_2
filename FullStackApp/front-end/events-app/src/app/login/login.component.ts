import { Component } from '@angular/core';
import { User } from 'src/user';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  loginUserData= new User("","")
   
   constructor(private _auth:AuthService, private _router:Router){}

  loginUser()
  {
      this._auth.loginUser(this.loginUserData).subscribe(
        res=>{
           localStorage.setItem('token',res.token)
          this._router.navigate(['/special']) 
        }
      )
    
    
  }
}
