import { Component } from '@angular/core';
import { User } from 'src/user';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  registerUserData= new User("","")

  constructor(private _auth: AuthService,private _router:Router){}

  registerUser()
  {
    this._auth.registerUser(this.registerUserData)
     .subscribe( res=>{
     localStorage.setItem('token',res.token)
      this._router.navigate(['/special'])
     }
    );

  }

}
