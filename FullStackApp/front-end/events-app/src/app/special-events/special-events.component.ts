import { Component } from '@angular/core';
import { Events } from 'src/events';
import { EventService } from '../event.service';

@Component({
  selector: 'app-special-events',
  templateUrl: './special-events.component.html',
  styleUrls: ['./special-events.component.css']
})
export class SpecialEventsComponent {
  specialEvents:Events[]=[]
     constructor(private _eventService:EventService){}

     ngOnInit()
     {
       this._eventService.getSpecialEvents().subscribe(
        res=>this.specialEvents=res
       )
     }
}
