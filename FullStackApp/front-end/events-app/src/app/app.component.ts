import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'events-app';
  constructor(private _router:Router){}
  loggedIn()
  {
    return !!localStorage.getItem('token')  
  }
  logout()
  {
    localStorage.removeItem('token');
    this._router.navigate(['login']);
  }
}
