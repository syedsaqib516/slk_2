package com.service2.contactService.beans;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Contacts 
{
	@Id
	Integer conId;
	String email;
	String phone;
	Integer userId;


}
