package com.gl.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gl.dto.User;

@RestController
@RequestMapping("/gl.com")
public class TestController {

	@GetMapping("/name")
	 public String getName()
	 {
		 return "saqib";
	 }
	
	@PostMapping("/user")
	 public User saveName(@RequestBody User user)
	 {
		 return user;
	 }
}
