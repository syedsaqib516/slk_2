package com.gl.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gl.dto.User;
import com.gl.util.FeignServerUtil;

@RestController
@RequestMapping("/gl.com")
public class ClientController {
	
	@Autowired
	FeignServerUtil feignServerUtil;
	
	@GetMapping("/username")
	public String getUserName()
	{
	  return  feignServerUtil.getName();
	}
	
	@PostMapping("/user")
	public User getUserName(@RequestBody User user)
	{
	  return  feignServerUtil.saveUser(user);
	}

}
