package com.gl.util;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.gl.dto.User;

@FeignClient(value="feignDemo", url="http://localhost:9090/gl.com")
public interface FeignServerUtil
{
	@GetMapping("/name")
     String getName();
	
	@PostMapping("/name")
    User saveUser(User user);
}
