package com.gl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class FeignDemoClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(FeignDemoClientApplication.class, args);
	}

}
