package com.hcl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.hcl.entities.Roles;
import com.hcl.entities.User;
import com.hcl.service.UserService;

@SpringBootApplication
public class Springsecurity3Application implements CommandLineRunner {

	@Autowired
	UserService service;
		
	@PostConstruct
		public void init()
		{
		System.out.println("executing post construct");
			Roles r1= new Roles(3,"CUSTOMER");
			
			List<Roles> roles= new ArrayList();
			roles.add(r1);
			
			
	      	User user =	new User(2,"cust","cust123",roles);
	      	service.userRegister(user);
		}
		
	public static void main(String[] args) {
		SpringApplication.run(Springsecurity3Application.class, args);

	}
	@Override
	public void run(String... args) throws Exception {
		//System.out.println(service.loadUserByUsername("saqib"));
		

	}

}
