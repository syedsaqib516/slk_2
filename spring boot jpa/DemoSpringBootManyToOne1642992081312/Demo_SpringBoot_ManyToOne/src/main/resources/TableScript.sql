DROP TABLE Customer CASCADE CONSTRAINTS;
DROP TABLE Loan CASCADE CONSTRAINTS;
DROP SEQUENCE hibernate_sequence;
CREATE SEQUENCE hibernate_sequence start with 2006 increment by  1;
CREATE TABLE Customer (
    customer_id number(10) NOT NULL,
    emailid varchar2(20),
    name varchar2(20),
    date_of_birth DATE,
    CONSTRAINT pk_customer PRIMARY KEY (customer_id)
);
CREATE TABLE Loan (
    loan_id number(10) NOT NULL,
    amount double precision,
    issue_date DATE,
    cust_id number(10),
    status varchar2(10),
    CONSTRAINT pk_loan PRIMARY KEY (loan_id),
    CONSTRAINT fk_cust_loan FOREIGN KEY (cust_id) REFERENCES Customer
);



INSERT INTO Customer VALUES (1001,'steven@infy.com', 'Steven Martin', SYSDATE-7476);
INSERT INTO Customer VALUES (1002,'kevin@infy.com', 'Kevin Nelson', SYSDATE-11374);
INSERT INTO Customer VALUES(1003,'john@infy.com', 'John Matric', SYSDATE-12344);
INSERT INTO Customer VALUES (1004,'chan@infy.com', 'Chan Mathew', SYSDATE-10344);
INSERT INTO Customer VALUES(1005,'jill@infy.com', 'Jill Mathew', SYSDATE-11374);

INSERT INTO Loan VALUES (2001,612345,SYSDATE-1000,1001,'Open');
INSERT INTO Loan VALUES (2002,2289073,SYSDATE-500,1001,'Closed');
INSERT INTO Loan VALUES (2003,109376289,SYSDATE-800,1001,'Open');

INSERT INTO Loan VALUES (2005,99027309,SYSDATE-2345,1004,'Open');

commit;

select * from Loan;
select * from Customer;

