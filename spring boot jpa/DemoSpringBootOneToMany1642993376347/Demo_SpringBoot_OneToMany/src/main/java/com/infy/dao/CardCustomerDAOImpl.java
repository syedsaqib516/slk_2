package com.infy.dao;

import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.infy.entity.CardEntity;
import com.infy.entity.CustomerEntity;
import com.infy.model.Card;
import com.infy.model.Customer;

@Repository(value = "cardCustomerDao")
public class CardCustomerDAOImpl implements CardCustomerDAO {

	@PersistenceContext
	private EntityManager entityManager;

	// adds a new card and  new customer
	@Override
	public Integer addCustomer(Customer customer) {
		Integer customerId = null;
		CustomerEntity customerEntity = new CustomerEntity();
		customerEntity.setName(customer.getName());
		customerEntity.setEmailId(customer.getEmailId());
		customerEntity.setDateOfBirth(customer.getDateOfBirth());
		List<Card> cardsToAllocate = customer.getCards();
		List<CardEntity> cards = new LinkedList<>();
		for (Card card : cardsToAllocate) {
			CardEntity newCard = new CardEntity();
			newCard.setCardId(card.getCardId());
			newCard.setCardNumber(card.getCardNumber());
			newCard.setExpiryDate(card.getExpiryDate());
			cards.add(newCard);
		}
		customerEntity.setCardEntities(cards);
		entityManager.persist(customerEntity);
		customerId = customerEntity.getCustomerId();
		return customerId;
	}

	// adds a new card to an existing customer
	@Override
	public void issueCardToExistingCustomer(Integer id, Card card) {
		CustomerEntity customer = entityManager.find(CustomerEntity.class, id);
		CardEntity cardEntity = new CardEntity();
		cardEntity.setCardId(card.getCardId());
		cardEntity.setCardNumber(card.getCardNumber());
		cardEntity.setExpiryDate(card.getExpiryDate());
		List<CardEntity> c = customer.getCardEntities();
		c.add(cardEntity);
		customer.setCardEntities(c);
	}

	// fetches the details of a particular customer
	@Override
	public Customer getCustomerDetails(Integer id) throws Exception {
		Customer customer = null;
		List<Card> cardDetails = new LinkedList<>();

		CustomerEntity customerEntity = entityManager.find(CustomerEntity.class, id);
		System.out.println(customerEntity);
		if (customerEntity != null) {
			customer = new Customer();
			customer.setEmailId(customerEntity.getEmailId());
			customer.setName(customerEntity.getName());
			customer.setCustomerId(customerEntity.getCustomerId());
			customer.setDateOfBirth(customerEntity.getDateOfBirth());
			List<CardEntity> cardEntities = customerEntity.getCardEntities();
			if (!cardEntities.isEmpty()) {
				for (CardEntity cardEntity : cardEntities) {
					Card card = new Card();
					card.setCardId(cardEntity.getCardId());
					card.setCardNumber(cardEntity.getCardNumber());
					card.setExpiryDate(cardEntity.getExpiryDate());
					cardDetails.add(card);
				}
			}
			customer.setCards(cardDetails);
		}
		return customer;
	}

	// deletes particular customer
	@Override
	public void deleteCustomer(Integer id) {
		CustomerEntity customer = entityManager.find(CustomerEntity.class, id);
		entityManager.remove(customer);
	}

	// deletes card of exiting customer
	@Override
	public void deleteCardOfExistingCustomer(Integer customerId, List<Integer> cardIdsToDelete) {
		CustomerEntity customerEntity = entityManager.find(CustomerEntity.class, customerId);
		for (Integer cardId : cardIdsToDelete) {
			CardEntity cardEntity = entityManager.find(CardEntity.class, cardId);
			customerEntity.getCardEntities().remove(cardEntity);
			entityManager.remove(cardEntity);
		}
	}
}
