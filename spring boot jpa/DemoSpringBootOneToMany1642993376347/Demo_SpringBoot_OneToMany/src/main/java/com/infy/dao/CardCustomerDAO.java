package com.infy.dao;

import java.util.List;

import com.infy.model.Card;
import com.infy.model.Customer;

public interface CardCustomerDAO {

	public Customer getCustomerDetails(Integer id) throws Exception;
	public Integer addCustomer(Customer customer);
	public void issueCardToExistingCustomer(Integer id, Card card);
	
	public void deleteCustomer(Integer id);
	public void deleteCardOfExistingCustomer(Integer customerId, List<Integer> cardIdsToDelete);	
	
	
		
	
		
	
}
