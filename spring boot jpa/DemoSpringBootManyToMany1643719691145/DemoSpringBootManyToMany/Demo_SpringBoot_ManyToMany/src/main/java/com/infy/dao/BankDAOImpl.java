package com.infy.dao;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.infy.entity.ServiceEntity;
import com.infy.entity.CustomerEntity;
import com.infy.model.Service;
import com.infy.model.Customer;

@Repository(value = "bankDAO")
public class BankDAOImpl implements BankDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Integer addCustomerAndService(Customer customer) throws Exception {
		Integer customerId = null;

		Set<Service> bankServices = customer.getBankServices();

		CustomerEntity customerEntity = new CustomerEntity();
		customerEntity.setDateOfBirth(customer.getDateOfBirth());
		customerEntity.setEmailId(customer.getEmailId());
		customerEntity.setName(customer.getName());

		Set<ServiceEntity> bankServicesEntities = null;

		if (bankServices != null && !bankServices.isEmpty()) {

			bankServicesEntities = new LinkedHashSet<ServiceEntity>();

			for (Service service : bankServices) {

				ServiceEntity servicesEntity = new ServiceEntity();
				servicesEntity.setServiceId(service.getServiceId());
				servicesEntity.setServiceName(service.getServiceName());
				servicesEntity.setServiceCost(service.getServiceCost());

				bankServicesEntities.add(servicesEntity);
			}

			customerEntity.setBankServices(bankServicesEntities);
		}
		
		entityManager.persist(customerEntity);
		
		customerId = customerEntity.getCustomerId();
		
		return customerId;
	}

	@Override
	public void addExistingServiceToExistingCustomer(Integer customerId,
			List<Integer> serviceIds) throws Exception {
		
		CustomerEntity customerEntity = entityManager.find(CustomerEntity.class, customerId);
		Set<ServiceEntity> servicesEntitySet = customerEntity.getBankServices();		
		for (Integer sId : serviceIds) {
			ServiceEntity servicesEntity = entityManager.find(ServiceEntity.class, sId);
			if (!servicesEntitySet.contains(servicesEntity)) {
				servicesEntitySet.add(servicesEntity);
			} 

		}
	}

	
	@Override
	public void deallocateServiceForExistingCustomer(Integer customerId,
			List<Integer> serviceIds) throws Exception {
		CustomerEntity customerEntity = entityManager.find(CustomerEntity.class, customerId);

		Set<ServiceEntity> existingServiceSet = customerEntity.getBankServices();
		
		for (Integer sId : serviceIds) {
			ServiceEntity serviceEntity=entityManager.find(ServiceEntity.class, sId);
			existingServiceSet.remove(serviceEntity);
			
		}
	}

	@Override
	public void deleteCustomer(Integer customerId) throws Exception {

		CustomerEntity customerEntity = entityManager.find(
				CustomerEntity.class, customerId);

		customerEntity.setBankServices(null);

		entityManager.remove(customerEntity);

	}

	@Override
	public Customer getCustomer(Integer customerId) throws Exception {
		Customer customer = null;
		CustomerEntity customerEntity = entityManager.find(
				CustomerEntity.class, customerId);

		if (customerEntity != null) {
			customer = new Customer();
			customer.setCustomerId(customerEntity.getCustomerId());
			customer.setDateOfBirth(customerEntity.getDateOfBirth());
			customer.setEmailId(customerEntity.getEmailId());
			customer.setName(customerEntity.getName());

			Set<ServiceEntity> bankServicesEntitySet = customerEntity
					.getBankServices();
			Set<Service> bankServicesSet = new LinkedHashSet<>();
			if (bankServicesEntitySet != null
					&& !bankServicesEntitySet.isEmpty()) {

				for (ServiceEntity bankServicesEnt : bankServicesEntitySet) {

					Service services = new Service();
					services.setServiceId(bankServicesEnt.getServiceId());
					services.setServiceName(bankServicesEnt.getServiceName());
					services.setServiceCost(bankServicesEnt.getServiceCost());

					bankServicesSet.add(services);
				}

				customer.setBankServices(bankServicesSet);
			}
		}
		return customer;
	}

	@Override
	public Service getService(Integer serviceId) throws Exception {
		ServiceEntity serviceEntity = entityManager.find(ServiceEntity.class,
				serviceId);
		Service service = null;
		if (serviceEntity != null) {
			service = new Service();
			service.setServiceId(serviceEntity.getServiceId());
			service.setServiceName(serviceEntity.getServiceName());
			service.setServiceCost(serviceEntity.getServiceCost());
			
		}
		return service;
	}

}