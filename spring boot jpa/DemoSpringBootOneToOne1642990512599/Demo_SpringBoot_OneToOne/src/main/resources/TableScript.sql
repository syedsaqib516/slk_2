DROP TABLE Customer CASCADE CONSTRAINTS;
DROP TABLE Address CASCADE CONSTRAINTS;

DROP SEQUENCE hibernate_sequence;
CREATE SEQUENCE hibernate_sequence START WITH 1006 INCREMENT BY 1;

CREATE TABLE Address(
	address_id NUMBER(14),
	street VARCHAR2(30) NOT null,
	city VARCHAR2(10) NOT NULL,
	CONSTRAINT ps_addressid_pk PRIMARY KEY (address_id)
);

insert into Address values(100,'8 East Walnut Street', 'New York');
insert into Address values(101,'720 Rockland Road', 'Las Vegas');
insert into Address values(102,'37 Marlborough Street', 'Gallup');

CREATE TABLE Customer (
	customer_id NUMBER(10),
	address_id NUMBER(12) UNIQUE,
	emailid VARCHAR2(25) NOT NULL,
	name VARCHAR2(10) NOT NULL,
	date_of_birth DATE NOT NULL,
	CONSTRAINT ps_customerid_pk PRIMARY KEY (customer_id),
	CONSTRAINT ps_customer_address_fk FOREIGN KEY(address_id) REFERENCES Address(address_id)
);



INSERT INTO Customer VALUES (1234,100,'james01@infosys.com', 'James', SYSDATE-7476);
INSERT INTO Customer VALUES (1235, 101,'william@infosys.com', 'William', SYSDATE-11374);
INSERT INTO Customer VALUES (1236, 102,'antony_04@infosys.com', 'Antony', SYSDATE-12344);


commit;

select * from Customer;
select * from Address;
