package com.infy.dao;

import com.infy.model.Address;

import com.infy.model.Customer;




public interface CustomerDAO {
	public Customer getCustomer(Integer customerId);
	public Integer addCustomer(Customer customer);
	
	public void updateAddress(Integer customerId, Address newAddress) throws Exception;
	public void deleteCustomer(Integer customerId);
}
