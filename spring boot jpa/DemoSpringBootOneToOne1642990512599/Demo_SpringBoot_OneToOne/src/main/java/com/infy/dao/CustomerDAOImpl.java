package com.infy.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.infy.entity.AddressEntity;

import com.infy.entity.CustomerEntity;
import com.infy.model.Address;

import com.infy.model.Customer;

@Repository(value = "customerDao")
public class CustomerDAOImpl implements CustomerDAO {
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Customer getCustomer(Integer customerId) {
		CustomerEntity customerEntity  = entityManager.find(CustomerEntity.class, customerId);
		Customer customer = new Customer();
		customer.setCustomerId(customerEntity.getCustomerId());
		customer.setName(customerEntity.getName());
		customer.setEmailId(customerEntity.getEmailId());
		customer.setDateOfBirth(customerEntity.getDateOfBirth());
		Address address = new Address();
		address.setAddressId(customerEntity.getAddressEntity().getAddressId());
		address.setCity(customerEntity.getAddressEntity().getCity());
		address.setStreet(customerEntity.getAddressEntity().getStreet());
		customer.setAddress(address);
		return customer;
	}

	@Override
	public Integer addCustomer(Customer customer) {

		CustomerEntity customerEntity = new CustomerEntity();
		customerEntity.setCustomerId(customer.getCustomerId());
		customerEntity.setDateOfBirth(customer.getDateOfBirth());
		customerEntity.setEmailId(customer.getEmailId());
		customerEntity.setName(customer.getName());
		
		AddressEntity addressEntity = new AddressEntity();
		addressEntity.setAddressId(customer.getAddress().getAddressId());
		addressEntity.setCity(customer.getAddress().getCity());
		addressEntity.setStreet(customer.getAddress().getStreet());
		
		customerEntity.setAddressEntity(addressEntity);
		entityManager.persist(customerEntity);
		return customerEntity.getCustomerId();
	}

	@Override
	public void updateAddress(Integer customerId, Address newAddress) throws Exception {
		CustomerEntity customerEntity  = entityManager.find(CustomerEntity.class, customerId);
		AddressEntity addressEntity = customerEntity.getAddressEntity();
		addressEntity.setCity(newAddress.getCity());
		addressEntity.setStreet(newAddress.getStreet());
		
	}

	@Override
	public void deleteCustomer(Integer customerId) {
		CustomerEntity customerEntity  = entityManager.find(CustomerEntity.class, customerId);
		entityManager.remove(customerEntity);
	}
}