package com.infy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.infy.dao.CustomerDAO;

import com.infy.model.Address;

import com.infy.model.Customer;

@Service(value = "customerService")
@Transactional
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerDAO customerDAO;

	@Override
	public Customer getCustomer(Integer customerId) throws Exception {
		Customer customer = customerDAO.getCustomer(customerId);
		if (customer == null)
			throw new Exception("SERVICE.INVALID_CUSTOMERID");
		else
			return customer;
	}

	@Override
	public Integer addCustomer(Customer customer) {
		return customerDAO.addCustomer(customer);
	}

	@Override
	public void updateAddress(Integer customerId, Address newAddress) throws Exception {
		Customer customer = customerDAO.getCustomer(customerId);
		if (customer == null)
			throw new Exception("SERVICE.INVALID_CUSTOMERID");
		else
			customerDAO.updateAddress(customerId, newAddress);
	}

	@Override
	public void deleteCustomer(Integer customerId) throws Exception {
		Customer customer = customerDAO.getCustomer(customerId);
		if (customer == null)
			throw new Exception("SERVICE.INVALID_CUSTOMERID");
		else
			customerDAO.deleteCustomer(customerId);

	}

}
