package com.infy.service;

import com.infy.model.Address;

import com.infy.model.Customer;


public interface CustomerService {

	public Customer getCustomer(Integer customerId) throws Exception;
	public Integer addCustomer(Customer customer);
	
	public void updateAddress(Integer customerId, Address newAddress) throws Exception;
	public void deleteCustomer(Integer customerId) throws Exception;
	
}
